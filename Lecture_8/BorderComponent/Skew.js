import React, { Component } from "react";
import { StyleSheet, Text, View, FlexContainer } from "react-native";

export default class APP extends Component {
  render() {
    return (
      <View style={styles.container}>
        {/* <Example>A</Example>
        <Example style={{ transform: [{ skewX: "45deg" }] }}>B X45</Example>
        <Example style={{ transform: [{ skewX: "-45deg" }] }}>C X-45</Example>
        <Example style={{ transform: [{ skewY: "45deg" }] }}>D Y45</Example>
        <Example style={{ transform: [{ skewY: "-45deg" }] }}>E Y-45</Example> */}
        <FlexContainer style={[{ justifyContent: "center" }]}>
           <Example>center</Example>
          <Example>center</Example>
        </FlexContainer>
      </View>
    );
  }
}

const Example = (props) => (
  <View style={[styles.example, props.style]}>
    <Text>{props.children}</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    alignItems: "center",
    flex: 1,
  },
  example: {
    width: 75,
    height: 75,
    borderWidth: 2,
    margin: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  flexContainer: {
    width: 150,
    height: 150,
    borderWidth: 1,
    margin: 10,
    flexDirection: "row",
  },
});
