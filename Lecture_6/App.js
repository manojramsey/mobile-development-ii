import { StatusBar } from "expo-status-bar";
import React from "react";
import { Button, StyleSheet, Text, View } from "react-native";
import ComponentA from "./components/component A/ComponentA";
import DarkTheme from "./components/LightAndDark/DarkTheme";

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={[styles.message, styles.warning]}>
        Open up App.js to start working on your app!
      </Text>
      <Text style={[{ color: "red" }, { color: "yellow" }]}>React Styles</Text>
      <ComponentA />
      <DarkTheme />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginLeft: 20,
    marginTop: 20,
  },
  message: {
    fontSize: 18,
  },
  warning: {
    color: "red",
  },
});
