import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Br from "./BorderComponent/Br";
import ProfileCard from "./BorderComponent/ProfileCard";
import Monospace from "./BorderComponent/Monospace";
import Skew from "./BorderComponent/Skew";

export default function App() {
  return (
    <View style={styles.container}>
      <Skew />
      {/* <Br /> */}
      {/* <ProfileCard /> */}
      {/* <Monospace /> */}
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
