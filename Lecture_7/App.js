import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Br from "./BorderComponent/Br";
import ProfileCard from "./BorderComponent/ProfileCard";

export default function App() {
  return (
    <View style={styles.container}>
      {/* <Br /> */}
      <ProfileCard />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
