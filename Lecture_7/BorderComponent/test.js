import React, { Component } from "react";
import PropTypes from "prop-types";
import update from "immutability-helper";
import {
  Platform,
  Image,
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
} from "react-native";

const userImage = require("./images/user.png");

const data = [
  {
    image: userImage,
    name: "John Doe",
    occupation: "React Native Developer",
    description:
      "John is a really good JS developer. " +
      "He likes using JS to build React " +
      "for IOS and Android",
    showThumbnail: true,
  },
];

const ProfileCard = (props) => {
  const {
    image,
    name,
    occupation,
    description,
    onPress,
    showThumbnail,
  } = props;
  let containerStyles = [styles.cardContainer];

  if (showThumbnail) {
    containerStyles.push(styles.cardThumbnail);
  }

  return (
    <TouchableHighlight onPress={onPress}>
      <View style={styles.cardContainer}>
        <View style={styles.cardImageContainer}>
          <Image style={styles.cardImage} source={image} />
          <View>
            <Text style={styles.cardName}>{name} </Text>
          </View>
          <View style={styles.cardOccupationContainer}>
            <Text style={styles.cardOccupation}>{occupation}</Text>
          </View>
          <View>
            <Text style={styles.cardDescription}>{description}</Text>
          </View>
        </View>
      </View>
    </TouchableHighlight>
  );
};

ProfileCard.propTypes = {
  image: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  occupation: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  showThumbnail: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default class App extends Component {
  constructor(props, context) {
    super(context);
    super(props);
    this.state = {
      data: data,
    };
  }
  handelProfileCardPress = (index) => {
    const showThumbnail = !this.state.data[index].showThumbnail;
    this.setState({
      data: update(this.state.data, {
        [index]: { showThumbnail: { $set: showThumbnail } },
      }),
    });
  };
  render() {
    const list = this.state.data.map(function (item, index) {
      const { image, name, occupation, description, showThumbnail } = item;
      return (
        <ProfileCard
          key={"card-" + index}
          image={image}
          name={name}
          occupation={occupation}
          description={description}
          onPress={this.handleProfileCardPress.bind(this, index)}
          showThumbnail={showThumbnail}
        />
      );
    }, this);
    return <View style={styles.container}>{list}</View>;
  }
}

const profileCardColor = "dodgerblue";

const styles = StyleSheet.create({
  cardContainer: {
    alignItems: "center",
    borderColor: "black",
    borderWidth: 3,
    borderStyle: "solid",
    borderRadius: 20,
    backgroundColor: profileCardColor,
    width: 300,
    height: 400,
    ...Platform.select({
      ios: {
        shadowColor: "black",
        shadowOffset: {
          height: 10,
        },
        shadowOpacity: 1,
      },
      android: {
        elevation: 15,
      },
    }),
  },
  cardImageContainer: {
    alignItems: "center",
    backgroundColor: "white",
    borderWidth: 3,
    borderColor: "black",
    width: 120,
    height: 120,
    borderRadius: 60,
    marginTop: 30,
    paddingTop: 15,
    ...Platform.select({
      ios: {
        shadowColor: "black",
        shadowOffset: {
          height: 10,
        },
        shadowOpacity: 1,
      },
      android: {
        borderWidth: 3,
        borderColor: "black",
        elevation: 15,
      },
    }),
  },
  cardImage: {
    width: 80,
    height: 80,
  },
  cardName: {
    color: "white",
    marginTop: 30,
    fontWeight: "bold",
    fontSize: 24,
  },
  cardOccupationContainer: {
    borderColor: "black",
    borderBottomWidth: 3,
  },
  cardOccupation: {
    fontWeight: "bold",
    marginTop: 10,
    marginBottom: 10,
  },
  cardDescription: {
    fontStyle: "italic",
    marginTop: 10,
    marginRight: 40,
    marginLeft: 40,
    marginBottom: 10,
  },
  cardThumbnail: { transform: [{ scale: 0.2 }] },
});
