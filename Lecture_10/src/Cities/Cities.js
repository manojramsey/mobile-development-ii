import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  ScrollView,
} from "react-native";

import CenterMessage from "../components/CenterMessage";
import { colors } from "../theme";

export default class Cities extends React.Component {
  static navigationOptions = {
    title: "Cities",
    headerTitleStyle: {
      color: "white",
      fontSize: 20,
      fontWeight: "400",
    },
  };
  navigate = (item) => {
    this.props.navigation.navigate("City", { city: item });
  };
  render() {
    return (
      <ScrollView>
        <View>
          {!Cities.length && <CenterMessage message="No Saved Cities!" />}
          {Cities.map((item, index) => (
            <TouchableWithoutFeedback
              onPress={() => this.navigate(item)}
              key={index}
            >
              <View style={styles.cityContainer}>
                <Text style={styles.city}>{item.city}</Text>
                <Text style={styles.country}>{item.country}</Text>
              </View>
            </TouchableWithoutFeedback>
          ))}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  cityContainer: {
    padding: 10,
    borderBottomWidth: 2,
    borderBottomColor: colors.primary,
  },
  city: {
    fontSize: 20,
  },
  country: {
    color: "rgba(0,0,0, .5)",
  },
});
